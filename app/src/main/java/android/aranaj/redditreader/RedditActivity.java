package android.aranaj.redditreader;


import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

public class RedditActivity extends SingleFragmentActivity implements ActivityCallback{

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment() {
        return new RedditListFragment();
    }

    @Override
    public void onPostSelected(Uri redditPostUri) {
        if(findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
            intent.setData(redditPostUri);
            startActivity(intent);
            MediaPlayer mp= MediaPlayer.create(getApplication(), R.raw.glass);
            mp.start();

        }
        else {
            Fragment detailFragment = RedditWebFragment.newFragment(redditPostUri.toString());
            getSupportFragmentManager().beginTransaction()
            .replace(R.id.detail_fragment_container, detailFragment)
            .commit();
        }
    }
}
